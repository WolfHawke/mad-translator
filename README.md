# Mad Translator
This is a silly little Python program which runs a string through Google Translate multiple times to get funny results.
It is super simple and based on the googletrans package. It has both a GUI and a command-line mode.

Mad Translator was mostly built to learn how to use the TKinter GUI interface.

Current Version: 0.3.1

## Obtaining Mad Translator
Mad Translator is available in three versions

* Python Source Code (recommended)
* Windows Executable in ZIP form
* Standalone Windows Executable

## Requirements
* Python 3.8 or higher
* Python Tkinter package -> see your OS Python distribution info for installation instructions

## Installing Python Source Version
1. Clone the repository.
2. Create a virtual environment with the command `python3 -m venv venv --prompt=mad-xlator`
3. Switch into the virtual environment with one of the following commands:
    * Windows PowerShell: `venv\Scripts\Activate.ps1`
    * All other systems: `source venv/bin/activate`
4. Either install the python modules using `pip install -r requirements.txt` or else install the app for direct execution using `pip install -e .`
    * To run Mad Translator as a standalone, you can run `python3 mad_translator`.
    * To run the direct exeution run `mad_translator`.

## Usage
GUI: `mad_translator`

CLI: `mad_translator --string="[STRING]" --times=[TIMES] --language=[LANGUAGE_CODE]`

**CLI Options**

* `--string="[STRING]"` - String to translate.
* `--times=[TIMES]` - How many times to translate a given string as an integer (e.g. 3). Minimum is 1, maximum is 200.
* `--language=[LANGUAGE_CODE]` - Two-letter code of what target language to use. It should be the same as the original language. Enter `qq` to automatically detect the language.
* `--help` or `-h` - Display help screen
* `--version` or `-v` - Display version information

## Compiling
When creating the virtual environment for Mad Translator, the pyinstaller package will automatically be installed. Here is how to compile Mad Installer for various platforms:

### Windows
For standalone mode to be zipped use the following at a command prompt (PowerShell recommended): 

`pyinstaller --add-data "assets\mad-translator.ico;assets" --add-data "assets\copy-icon-square-24.png;assets" --icon="assets\mad-translator.ico" .\mad_translator.py`

For a single file version add the `--onefile` option before the `\mad.translator`.

**Note:** Because of the stupidity of Windows 10/11 you cannot use the `-w` and `--onefile` flags in the command line together. Windows will detect the resulting EXE as having a trojan virus in it, even though it doesn't. You can build your own bootloader from source to circumvent this, but for simplicity sake, just leave off the `-w` flag and it will work fine.

### MacOS
Before compiling the file, open gui.py and change line 81 to

`self.iconbitmap(resourcePath('assets/mad-translator.icns'))`

The run the following command from a terminal:

`pyinstaller --add-data "assets/mad-translator.icns;assets" --add-data "assets/copy-icon-square-24.png;assets" --icon="assets/mad-translator.icns" -w --onefile \mad_translator.py`

*Note that the icon files are .icns files rather than .ico!*

### Running Compiled Versions
Compiled versions can be either run by double-clicking their executable icon in Explorer or in Finder; or else they can be run from the command line using PowerShell or Terminal. If you use the CLI, you can use the flags to run the command-line version rather than the GUI version.


## History
* 0.3.1 - addtions and updates to make pyinstaller compiling possible.
* 0.3.0 - Engine and GUI rewritten so that cancel button and copy buttons work.
* 0.2.0 - Tkinter GUI added
* 0.1.0 - Initial CLI version

## Credits
Written by JMW

Copyright © 2022 Hawke AI and TBNRSonicMax

## License
[Creative Commons BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)

