#!/user/bin/env python3
# -*- coding: utf-8 -*-
# ================================================================================
"""
    File name: engine.py
    Date Created: 2022-03-28
    Date Modified: 2022-05-06
    Python version: 3.8
"""
__title__ = "Mad Translator"
__author__ = "Josh Wibberley (JMW)"
__copyright__ = "Copyright 2022, Hawke AI"
__credits__ = ["Josh Wibberley"]
__license__ = "CC BY-NC-SA 4.0"
__version__ = "0.3.1"
__maintainer__ = "Josh Wibberley"
__email__ = "jmw@hawke-ai.com"
__status__ = "Production"
# ================================================================================

from random import randint
from math import floor
from calculate_percentage import Percentage
import googletrans, sys, os

def calculateProgress(current, total):
    """
    Calculates how far we are in the progress
    """
    ppercent = round((current/total)*100, 1)


def cliProgressBar(current, total, label="Progress", last=False):
    """
    prints out a command line progress bar

    :param current   : Current item being processed
    :type  current   : integer
    :param total     : Total number of items
    :type  total     : integer
    :param label     : An optional label to print at the beginning of 
                       the progress bar
    :type  label     : string
    :param last      : Defines if this is the final (100%) iteration
    :type  last      : boolean
    """
    space = '.' #u' '
    box = '#' #u'\u2588'
    bar = '{label}: [{progress}] {percent}%'

    # make sure that the last one always shows 100%
    if last:
        current = total

    # calculate percentage
    percent = Percentage(current, total)

    # 2% per block (50 chars = 100%)
    progress = floor(percent.percent_int/2)
    out = bar.format(label=label, progress=(box*progress + space*(50-progress)), percent=percent.percent_long)

    sys.stdout.write(out)
    sys.stdout.flush()
    if last:
        sys.stdout.write('\n')
    else:
        sys.stdout.write('\r')
        
    sys.stdout.flush()


def genLangList(times=30, targetLang=''):
    """
    generate list of languages to translate through

    :param times     : how many languages to translate into
    :type  times     : 
    :param targetLang: language to translate into
    :type  targetLang: string
    """
    if targetLang == '' or type(times) is not int:
        return False

    # make sure we have a certain number of times
    if times < 1:
        times = 1     # in real life this will translate it twice
    elif times > 199: 
        times = 199   # 199 is the max, because it will result in 200 translations total
    
    # pull available two-letter language codes from googletrans.LANGOCDES
    langCodes = []
    for item in googletrans.LANGCODES.items():
        langCodes.append(item[1])
    
    # get {times} number of random language codes to go throuhg
    madCodes = []
    for item in range(0,times):
        madCodes.append(langCodes[randint(0,len(langCodes)-1)])

    madCodes.append(targetLang)

    return madCodes


def resourcePath(relative_path):
    """ 
    Get absolute path to resource, works for dev and for PyInstaller 
    Swiped from: https://stackoverflow.com/questions/51060894/adding-a-data-file-in-pyinstaller-using-the-onefile-option

    :param relative_path: path to file to append
    :type  relative_path: string
    """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


def madTransCli(src_str, times=30, lang='qq'):
    """
    function that does the bulk translation of the string

    :param src_str: string to translate
    :type  src_str: string
    :param times  : how many times to translate the string (default 30)
    :type  times  : integer
    :param lang   : input and output language in 2-letter language code; qq = detect langauge
    :type  lang   : string
    """
    x = googletrans.Translator()

    # detect the language
    if lang == 'qq':
        lang = x.detect(src_str).lang
    
    madCodes = genLangList(times=times, targetLang=lang)

    mad_string = ''

    for step, item in enumerate(madCodes):
        cliProgressBar(step, times, label="Processing")

        if mad_string == '':
            mad_string = x.translate(src_str, dest=item)
        else:
            mad_string = x.translate(mad_string.text, dest=item)

    cliProgressBar(times, times, label="Processed", last=True)

    return mad_string.text.lower()

