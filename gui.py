#!/user/bin/env python3
# -*- coding: utf-8 -*-
# ================================================================================
"""
    File name: gui.py
    Date Created: 2022-04-02
    Date Modified: 2022-05-06
    Python version: 3.8
"""
__title__ = "Mad Translator"
__author__ = "Josh Wibberley (JMW)"
__copyright__ = "Copyright © 2022 Hawke AI and TBNRSonicMax"
__credits__ = ["Josh Wibberley"]
__license__ = "CC BY-NC-SA 4.0"
__version__ = "0.3.1"
__maintainer__ = "Josh Wibberley"
__email__ = "jmw@hawke-ai.com"
__status__ = "Production"
# ================================================================================

import tkinter as tk
from tkinter import ttk
from tkinter.scrolledtext import ScrolledText
from tk_tooltip import CreateToolTip
from googletrans import LANGCODES, LANGUAGES, Translator
import webbrowser
from calculate_percentage import Percentage
from engine import genLangList, resourcePath


class MadGui(tk.Tk):
    """
    The tkinter-based GUI
    
    :param defaultLang: two-letter default language code (default=qq)
    :type  defaultLang: string
    """

    # global properties
    cancelTranslate = False
    currentData = []
    currentStep = 0
    madList = []
    totalSteps = 0
    xlator = Translator()
    
    # global methods
    def __init__(self, defaultLang='qq'):
        super().__init__()

        # configure the GUI
        self.title(__title__)

        # column configuration (8x8 grid)
        columns = 15
        rows = 6

        for item in range(columns):
            self.columnconfigure(item, weight=1)
            #self.rowconfigure(item, weight=1)

        for item in range(rows):
            self.rowconfigure(item, weight=1)
        
        self.rowconfigure(rows+1, weight=2)

        # position window
        window_width = 800
        window_height = 400

        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()

        center_x = int(screen_width/2 - window_width/2)
        center_y = int(screen_height/2 - window_height/2)
        
        self.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')
        self.resizable(False,False)

        # add icon
        self.iconbitmap(resourcePath('assets/mad-translator.ico'))

        # window contents
        self.lbl_welcome = ttk.Label(self, text='Welcome to Mad Translator!', font=('Cambria', 14))
        self.lbl_welcome.grid(column=0, row=0, padx=5, pady=5, columnspan=columns)

        self.lbl_description = ttk.Label(self, text='Run strings through Google Translate multiple times to come up with hilarious results.')
        self.lbl_description.grid(column=0, row=1, padx=5, pady=5, columnspan=columns)

        self.lbl_string = ttk.Label(self, text="String to Translate:")
        self.lbl_string.grid(column=0, row=3, sticky=tk.EW, columnspan=3, padx=10, pady=5)
        
        
        self.str_to_xlate = tk.StringVar()
        self.entry_string = ttk.Entry(self, textvariable=self.str_to_xlate)
        self.entry_string.grid(column=3, row=3, sticky=tk.EW, columnspan=11, padx=5, pady=5)
        CreateToolTip(self.entry_string, text='Enter word or phrase to translate. Translation will not work if left empty!')

        self.lbl_language = ttk.Label(self, text="Select Language:")
        self.lbl_language.grid(column=0, row=4, sticky=tk.EW, columnspan=3, padx=10, pady=5)
        self.str_language = tk.StringVar(value='Detect Language')
        
        langlist = ['Detect Language']
        LANGCODES['detect language'] = 'qq'
        for item in LANGCODES.keys():
            if LANGCODES[item] == defaultLang:
                self.str_language = tk.StringVar(value=item.title())
            langlist.append(item.title())

        self.combo_language = ttk.Combobox(self, textvariable=self.str_language, values=langlist)
        self.combo_language.grid(column=3, row=4, sticky=tk.EW, columnspan=3, padx=5, pady=5)

        self.lbl_times = ttk.Label(self, text="Times to Translate:")
        self.lbl_times.grid(column=6, row=4, sticky=tk.EW, columnspan=4, padx=10, pady=5)
        self.current_times = tk.StringVar(value=30)
        self.spin_times = ttk.Spinbox(self, from_=1, to=200, textvariable=self.current_times, wrap=True)
        self.spin_times.grid(column=9, row=4, padx=5, pady=5)
        CreateToolTip(self.spin_times, text='Minimum=1; maximum=200')

        self.btn_go = ttk.Button(self, text="Translate", command=self.doGuiTranslation)
        self.btn_go.grid(column=12, row=4, columnspan=2, sticky=tk.E, padx=5, pady=5)
        CreateToolTip(self.btn_go, text='Click here to begin translation')

        self.progress_frame = ttk.LabelFrame(self, text="Translation Progress:")
        self.progress_frame.columnconfigure(0, weight=1)
        self.progress_frame.grid(column=0, row=5, columnspan=15, sticky=tk.EW, padx=10, pady=20)

        self.progress_bar = ttk.Progressbar(self.progress_frame, length=600, maximum=100)
        self.progress_bar.grid(column=0, row=0, sticky=tk.W, padx=10, pady=10)
        self.progress_text = ttk.Label(self.progress_frame, text="")
        self.progress_text.grid(column=1, sticky=tk.E, row=0, padx=10, pady=10)
        self.progress_cancel = ttk.Button(self.progress_frame, text="Cancel", command=self.cancelProgress, state='disabled')
        self.progress_cancel.grid(column=2, row=0, padx=10, pady=10)
        

        self.result_frame = ttk.LabelFrame(self, text="Result:")
        self.result_frame.columnconfigure(0, weight=10)
        self.result_frame.columnconfigure(1, weight=1)
        self.result_frame.grid(column=0, row=6, sticky=tk.EW, columnspan=15, padx=10, pady=5)
        
        self.text_result = ScrolledText(self.result_frame, width=40, height=3)
        self.text_result.grid(column=0, row=0, sticky=tk.EW, padx=10, pady=10)
        
        self.copy_icon = tk.PhotoImage(file=resourcePath('assets/copy-icon-square-24.png'))
        self.btn_copy = ttk.Button(self.result_frame, image=self.copy_icon, state='disabled', command=self.copyResult)
        self.btn_copy.grid(column=1, row=0, columnspan=8, padx=5, pady=5, sticky=tk.E)
        CreateToolTip(self.btn_copy, 'Copy result to clipboard')

        self.lbl_copyright= ttk.Label(self, text=__copyright__, font=('', 7))
        self.lbl_copyright.grid(column=0, row=7, sticky=tk.SW, columnspan=7, padx=10, pady=5)
        self.lbl_copyright.bind('<ButtonRelease>', self.loadCopyrightSite)

        self.lbl_license = ttk.Label(self, text=f"License: {__license__} ", font=('', 7))
        self.lbl_license.grid(column=10, row=7, columnspan=5, sticky=tk.SE, padx=10, pady=5)
        self.lbl_license.bind('<ButtonRelease>', self.loadLicenseSite)


    def loadCopyrightSite(self, cmd):
        """
        Load the website of the copyright holder
        """
        webbrowser.open('https://www.hawke-ai.com')


    def loadLicenseSite(self, cmd):
        """
        Load the website explaining the license
        """
        webbrowser.open('https://creativecommons.org/licenses/by-nc-sa/4.0/')


    def cancelProgress(self):
        """
        Cancel translation progress
        """
        self.cancelTranslate = True
        self.writeResult("Cancel request acknowledged.", copy=False)


    def clearResult(self):
        """
        Clears any text in the result field
        """
        self.text_result.delete('1.0', tk.END)
        self.text_result.update_idletasks()
        self.btn_copy['state'] = 'disabled'
        self.btn_copy.update_idletasks()


    def copyResult(self):
        self.clipboard_clear()
        self.clipboard_append(self.text_result.get('1.0',tk.END))


    def doGuiTranslation(self):
        """
        Execute the translation
        """
        if len(self.str_to_xlate.get()) > 0:
            self.clearResult()
            self.cancelTranslate = False
            self.translating = True
            self.progress_cancel['state'] = 'enabled'
            self.btn_go['state'] = 'disabled'
            
            self.currentData = {"text": self.str_to_xlate.get(), "from":"", "to":""}

            # detect source and target language
            if LANGCODES[self.str_language.get().lower()] == 'qq':
                self.currentData['from'] = self.xlator.detect(self.currentData['text']).lang
            else:
                self.currentData['from'] = LANGCODES[self.str_language.get().lower()]

            self.madList = genLangList(times=int(self.current_times.get()), targetLang=self.currentData['from'])
            self.currentData['to'] = self.madList[0]
            self.totalSteps = len(self.madList)

            self.madTranslateStep()  # step into recursion

        else:
            self.writeResult('Please enter a string to translate.', copy=False)


    def madTranslateStep(self):
        """
        recursive function to run the translation
        """
        if not self.cancelTranslate:
            p = Percentage(self.currentStep, self.totalSteps)
            self.updateProgressBar(p)

            if self.currentStep < self.totalSteps:
                x = self.xlator.translate(self.currentData['text'], dest=self.currentData['to'], src=self.currentData['from'])
                
                # increment data for next iteration
                self.currentStep = self.currentStep + 1
                self.currentData['text'] = x.text
                self.currentData['from'] = self.currentData['to']

                # only iterate to data if we
                if self.currentStep < self.totalSteps:
                    self.currentData['to'] = self.madList[self.currentStep]
                
                # trigger next iteration
                self.after(250, self.madTranslateStep)
                
            else:
                self.writeResult(self.currentData['text'])
                self.reset()

        else:
            self.writeResult('Translation was cancelled', copy=False)
            self.reset()


    def reset(self):
        """
        Reset global variables and progress bar
        """
        self.currentStep = 0
        self.totalSteps = 0
        self.cancelTranslate = 0
        self.currentData['from'] = ''
        self.currentData['to'] = ''
        self.progress_bar['value'] = 0
        self.progress_text['text'] = ''
        self.progress_cancel['state'] = 'disabled'
        self.btn_go['state'] = 'enabled'
        self.update_idletasks()


    def updateProgressBar(self, perc):
        """
        Updates the progress bar with a Percentage object
        
        :param perc: percentage data
        :type  perc: Percentage class
        """
        self.progress_bar['value'] = perc.percent_int
        self.progress_bar.update_idletasks()
        self.progress_text['text'] = '{}%'.format(str(perc.percent_long))
        self.progress_text.update_idletasks()


    def writeResult(self, text, copy=True):
        """
        Write text to the result screen

        :param text: text of result to write
        :type  text: string
        :param copy: whether or not to enable the copy button
        """
        self.text_result.delete('1.0', tk.END)
        self.text_result.insert(tk.END, text)
        self.text_result.update_idletasks()

        if copy:
            self.btn_copy['state'] = 'enabled'
            self.btn_copy.update_idletasks()

