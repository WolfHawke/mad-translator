#!/user/bin/env python3
# -*- coding: utf-8 -*-
# ================================================================================
"""
    File name: mad_translator.py
    Date Created: 2022-03-28
    Date Modified: 2022-05-06
    Python version: 3.8
"""
__title__ = "Mad Translator"
__author__ = "Josh Wibberley (JMW)"
__copyright__ = "Copyright 2022, Hawke AI"
__credits__ = ["Josh Wibberley"]
__license__ = "CC BY-NC-SA 4.0"
__version__ = "0.3.1"
__maintainer__ = "Josh Wibberley"
__email__ = "jmw@hawke-ai.com"
__status__ = "Production"
# ================================================================================

from engine import madTransCli
from gui import MadGui
import click

# script defaults
timesToTranslate = 30
targetLang = 'qq'

# get command line arguments
@click.command()
@click.option('--string', 'str_to_xlate', default='', help='String to translate', type=str)
@click.option('--times', default=timesToTranslate, help='How many times to translate a given string.', type=int)
@click.option('--language', default=targetLang, type=str, help='What target language to use. It should be the same as the original language. Enter qq to automatically detect the language.')
@click.version_option(__version__, prog_name='Mad Translator')
def doTranslation(str_to_xlate, times, language):   
    """Mad Translator 
    © 2022 Hawke AI and TBNRSonicMax; CC BY-NC-SA 4.0
    
    Translate a given string multiple times with Google Translate to come up 
    with some really funny results.

    Running the command without arguments will execute a GUI.
    Use the parameters below to run the command from the command line.
    """ 
    if str_to_xlate == '':
        xlation = MadGui()
        xlation.mainloop()
    else:
        print('Welcome to Mad Translator!')
        print('© 2022 Hawke AI and TBNRSonicMax; CC BY-NC-SA 4.0')
        print(f'Translating “{str_to_xlate}” {times} times.')
        xlation = madTransCli(str_to_xlate, times, language)
        print(' ')
        print(f'Result: {xlation}')

if __name__ == '__main__':
    doTranslation()