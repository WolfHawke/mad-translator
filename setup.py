from setuptools import setup

setup(
    name='mad_translator',
    version='0.3.0',
    py_modules='mad_translator',
    install_requires=[
        'wheel',
        'click==8.1.2',
        'googletrans==4.0.0rc1',
        'pyinstaller==5.0.1'
    ],
    entry_points={
        'console_scripts': [
            'mad_translator=mad_translator:doTranslation'
        ]
    }
)
