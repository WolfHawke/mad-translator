#!/user/bin/env python3
# -*- coding: utf-8 -*-
# ================================================================================
"""
    File name: calculate_percentage.py
    Date Created: 2022-04-30
    Date Modified: 2022-04-30
    Python version: 3.8
"""
__title__ = "Calculate Percentage Class"
__author__ = "Josh Wibberley (JMW)"
__copyright__ = "Copyright 2022, Hawke AI"
__credits__ = ["Josh Wibberley"]
__license__ = "CC BY-SA 4.0"
__version__ = "1.0"
__maintainer__ = "Josh Wibberley"
__email__ = "jmw@hawke-ai.com"
__status__ = "Production"
# ================================================================================
from math import floor


class Percentage(object):
    """
    Calculate a percentage between two integers
    
    :param increment: The amount of the total that is used
    :type  increment: integer
    :param total    : The total amount to compare the amount to
    :param increment: integer
    """

    def __init__(self, increment, total):
        """
        Class initialization function
        """
        self.err = []

        try:
            self.increment = float(increment)
        except ValueError:
            self.err.append("Invalid increment")
            self.increment = float(0)

        try:
            self.total = float(total)
        except ValueError:
            self.err.append("Invalid total")
            self.total = float(0)

        try:
            self.percent_actual = increment/total
        except TypeError:
            self.percent_actual = float(0)

        self.percent_long = round(self.percent_actual*100, 1)
        self.percent_int = int(floor(self.percent_long))


    def __str__(self):
        """
        Render class content as string
        """
        if len(self.err) == 0:
            self.err = None

        tpl = "Percentage <increment: {i}, total: {t}, precent_actual: {pa}, percent_long: {pl}, percent_int: {pi}, errors: {e}>"
        return tpl.format(i=self.increment, t=self.total, pa=self.percent_actual, pl=self.percent_long, pi=self.percent_int, e=str(self.err))


    def __repr__(self):
        """
        Define class type
        """
        return "<Percentage Object>"
